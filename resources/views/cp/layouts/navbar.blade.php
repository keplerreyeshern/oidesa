


<nav class="navbar navbar-expand-md  -topnav">


        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="/cpanel/assets/brand/logo.svg" class="img-fluid" width="120px">
        </a>




        <div class="collapse navbar-collapse" id="navbarSupportedContent">


            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->



                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right -logout text-center" aria-labelledby="navbarDropdown">
                            <a href="{{ url('/logout') }}" > logout </a>
                        </div>
                    </li>

            </ul>
        </div>

</nav>
