


<div class="-sidebar">


    <nav id="sidebar">


        <div class="">

            <ul class="list-unstyled">

                <li>
                    <a href="/cp/users"><i class="far fa-user"></i> Usuarios</a>
                </li>

                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Productos</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="/cp/productos"><i class="fas fa-store"></i> Productos</a>
                        </li>
                        <li>
                            <a href="/cp/producto-categorias"><i class="fas fa-tag"></i> Categorias</a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="/cp/galerias"><i class="fas fa-camera-retro"></i> Galerias</a>
                </li>
{{--                <li>--}}
{{--                    <a href="{{ route('cp.releases.index') }}"><i class="far fa-comment"></i> Comunicados</a>--}}
{{--                </li>--}}


            </ul>
        </div>
    </nav>
</div>

