<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} Control Panel</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-189021059-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-189021059-1');
    </script>


    <script async src="https://www.googletagmanager.com/gtag/js?id=G-K9RJJVTRT5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-K9RJJVTRT5');
    </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('cpanel/css/cp.css') }}" rel="stylesheet">
    <!-- Styles -->
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}


    <!-- DropZone -->
    <script src="{{  asset('cpanel/js/dropzone.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('cpanel/css/dropzone.css') }}">




</head>
<body>
<div id="app">


    @include('cp.layouts.navbar')

    <div class="container-fluid">
        <div class="row">
            <div class="-sidebar -side">
                @include('cp.layouts.sidebar')
            </div>


            <div class="col -contenido">
                @include('cp.parciales.titulo')
                @yield('content')
            </div>

        </div>
    </div>



</div>


</body>
<script type="text/javascript">
    Dropzone.options.dropzone =
        {
            maxFilesize: 10,

            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 60000,
            success: function (file, response) {
                console.log(response);
            },
            error: function (file, response) {
                return false;
            }
        };
</script>

<script src="/cp/js/preview.js"></script>

</html>
