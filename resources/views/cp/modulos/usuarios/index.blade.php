@extends('cp.layouts.cp')


@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a href="{{ route('cp.users.create') }}" class="-btn-agregar"><i class="fas fa-user-plus"></i> Agregar usuario</a>
            </div>
        </div>

        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ $data['modulo'] }}
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th width="15%">Acciones</th>
                            </thead>
                            <tbody>
                            @foreach($users as $user)

                                <tr>
                                    <td><a href="{{ route('cp.users.edit', ['id' => $user->id]) }}"  title="Editar">{{ $user->name }}</a></td>
                                    <td>{{ $user->email }} </td>
                                    <td>
                                        @if($user->active)
                                            <a href="{{ route('cp.users.active', ['id' => $user->id]) }}"><i class="fas fa-power-off -on"></i></a>
                                        @else
                                            <a href="{{ route('cp.users.active', ['id' => $user->id]) }}"><i class="fas fa-power-off -off"></i></a>
                                        @endif

                                        <a href="{{ route('cp.users.destroy', ['id' => $user->id]) }}" class="text-danger">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>


                        <div class="-paginacion">1 - 2 - 3 <- inactivo</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
