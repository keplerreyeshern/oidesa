@extends('cp.layouts.cp')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a href="{{ route('cp.releases.create') }}" class="-btn-agregar"><i class="fas fa-user-plus"></i> Agregar Comunicado</a>
            </div>
        </div>

        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        {{ $data['modulo'] }}
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <th>Imagen</th>
                                <th>Titulo</th>
                                <th>Autor</th>
                                <th>Descripción</th>
                                <th width="15%">Acciones</th>
                            </thead>
                            <tbody>
                            @foreach($releases as $release)

                                <tr>
                                    <td width="200px">
                                        @if($release->photo_list)
                                            <img src="{{$release->photo_list}}" class="img-fluid" alt="{{$release->photo_list}}">
                                        @else
                                            <img src="{{ asset('images/no-image-icon.png') }}" class="img-fluid" alt="Sin imagen">
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('cp.releases.edit', ['release' => $release->id]) }}"  title="Editar">
                                            {{ $release->title }}
                                        </a>
                                    </td>
                                    <td>{{ $release->author }} </td>
                                    <td>{{ $release->description }} </td>
                                    <td>
                                        @if($release->outstanding)
                                            <a href="{{ route('cp.releases.outstanding', ['id' => $release->id]) }}" class="text-warning">
                                                <i class="fas fa-star"></i>
                                            </a>
                                        @else
                                            <a href="{{ route('cp.releases.outstanding', ['id' => $release->id]) }}" class="text-info">
                                                <i class="far fa-star"></i>
                                            </a>
                                        @endif
                                        </a>
                                        <a href="{{ route('cp.users.active', ['id' => $release->id]) }}">
                                            @if($release->active)
                                                <i class="fas fa-power-off -on"></i>
                                            @else
                                                <i class="fas fa-power-off -off"></i>
                                            @endif
                                        </a>

                                        <a href="{{ route('cp.users.destroy', ['id' => $release->id]) }}" class="text-danger">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>


                        <div class="-paginacion">1 - 2 - 3 <- inactivo</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
