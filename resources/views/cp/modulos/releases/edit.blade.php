@extends('cp.layouts.cp')


@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a href="{{ route('cp.releases.index') }}" class="-btn-agregar"><i class="fas fa-list"></i> Lista</a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ $data['modulo'] }}
                    </div>
                    <release-cred-component :categories="{{$categories}}"
                                            action="edit"
                                            :releaseo="{{$release}}"
                                            :imagesall="{{ $images }}"
                                            :galleriesall="{{ $galleries }}">
                    </release-cred-component>
                </div>
            </div>
        </div>
    </div>

@endsection
