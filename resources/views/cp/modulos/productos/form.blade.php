@extends('cp.layouts.cp')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a href="{{ url()->previous() }}" class="-btn-agregar"><i class="fas fa-user-plus"></i> Listado</a>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                         {{  $data['modulo'] }}
                    </div>

{{--                    <form action="{{ route('cp.productos.store') }}" method="POST">--}}
{{--                        @csrf--}}
{{--                        <input type="hidden" name="name" value="Producto">--}}
{{--                        <input type="hidden" name="key" value="Producto">--}}
{{--                        <input type="hidden" name="model" value="Producto">--}}
{{--                        <input type="hidden" name="descriptions" value="Producto">--}}
{{--                        <input type="hidden" name="details" value="Producto">--}}
{{--                        <input type="hidden" name="measuresCms" value="Producto">--}}
{{--                        <input type="hidden" name="measuresInches" value="Producto">--}}
{{--                        <input type="submit" value="agregar">--}}
{{--                    </form>--}}
                    <div class="card-body">
                        <product-create-component :categoriesall="{{ $categories }}"></product-create-component>

                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
