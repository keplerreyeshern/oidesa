@extends('cp.layouts.cp')

@section('content')
        <products-component :productsall="{{ $products }}"
                            :categoriesall="{{ $categories }}"
                            :galleriesall="{{ $galleries }}"
                            :categoriesselectall="{{ $categoriesSelect }}"
                            :imagesall="{{ $images }}">
        </products-component>
    </div>
@endsection
