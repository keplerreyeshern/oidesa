@extends('cp.layouts.cp')

@section('content')

    <categories-component :data="{{$categories}}"></categories-component>

@endsection
