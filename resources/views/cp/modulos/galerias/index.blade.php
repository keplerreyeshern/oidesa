
@extends('cp.layouts.cp')

@section('content')
    <galleries-component :galleriesall="{{ $galleries }}"
                         :imagesall="{{ $images }}">
    </galleries-component>

@endsection
