@extends('cp.layouts.cp')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Panel de control') }} </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                           <h2> {{ __('Bienvenido! ') }}  {{ Auth::user()->name }} {{ __('a tu panel de control') }}</h2>

                            <h5>
                                etiquetas<i class="fas fa-tag"></i>
                               productos<i class="fas fa-store"></i>
                               imagenes<i class="fas fa-camera-retro"></i>
                                quienes somos<i class="fas fa-user-tie"></i>
                                parrafos<i class="fas fa-clipboard-check"></i>
                                Novedades<i class="far fa-star"></i>
                            </h5>



                            <table class="table table-striped table-hover">
                                <tbody>
                                <tr>
                                    <td>Col1</td>
                                    <td>Col 2 </td>
                                </tr>
                                <tr>
                                    <td>Col 1</td>
                                    <td>Col 2 </td>
                                </tr>
                                <tr>
                                    <td>Col 1</td>
                                    <td>Col 2 </td>
                                </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
