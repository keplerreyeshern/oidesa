@extends('public.layouts.app')
@section('titulo', $category->name)
@section('content')

    <div class="container-fluid -quienes-somos pr-0 p-0">

        <div class="-titulo-area text-right">
            <h1>{{ $data['titulo'] }}</h1>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $category->name }}</li>
            </ol>
        </nav>



        <div class="container-fluid tm-0">
            <div class="col-3">
                <div class="-title text-center">
                    <h4><span class="-blanco">{{ $category->name }}</span></h4>
                </div>
            </div>
            <div class="row px-5 mt-3">
                @foreach($products as $product)
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="card bg-dark p-2 mb-4">
                            <div class="card-image">
                                <div class="image">
                                    <a href="{{ route('product', ['category' => $category->slug, 'product' => $product->slug]) }}" class="">
                                        <img src="{{  $product->photo }}" class="-image" alt="{{ $product->photo }}">
                                    </a>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <h2 class="-title-product">
                                            {{ $product->name }}
                                        </h2>
                                    </div>
                                </div>
                                <div class="content card-product">
                                    <p>
                                        {!! $product->description == 'null' ? '': $product->description !!}
                                    </p>
                                </div>
                                <div class=" d-flex justify-content-end">
                                    <a href="{{ route('product', ['category' => $category->slug, 'product' => $product->slug]) }}" class="">
                                        Ver más
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                    <div class="col-12 mt-3 mb-5">
                        <div class="row mt-2">
                            @if($category->slug == 'brazos' || $category->slug == 'sillas' || $category->slug == 'sillones' || $category->slug == 'bancas')
                                <div class="col-1">
                                    <a href="{{ route('full-leather')}}">
                                        <img src="{{ asset('images/garantia-piel-integral.jpg') }}" class="img-fluid" alt="Garantia Piel Integral">
                                    </a>
                                </div>
                            @endif
                            @if($category->slug == 'asientos-automotriz' || $category->slug == 'cascos' || $category->slug == 'respaldos' || $category->slug == 'moldeados-flexibles' || $category->slug == 'sillones')
                                <div class="col-1">
                                    <a href="{{ route('polyurethane') }}">
                                        <img src="{{ asset('images/garantia-poliuretano.jpg') }}" class="img-fluid" alt="Garantia-poliuretano">
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
            </div>
        </div>
    </div>
    @include('public.parciales.home.elegirnos')
@endsection
