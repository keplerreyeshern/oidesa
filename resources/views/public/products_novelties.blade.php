@extends('public.layouts.app')
@section('titulo', 'Novedades')
@section('content')

    <div class="container-fluid -quienes-somos pr-0 p-0">

        <div class="-titulo-area text-right">
            <h1>{{ $data['titulo'] }}</h1>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Novedades</li>
            </ol>
        </nav>



        <div class="container-fluid tm-0">
            <div class="col-3">
                <div class="-title text-center">
                    <h4><span class="-blanco">Novedades</span></h4>
                </div>
            </div>
            <div class="row mt-3 px-5">
                @foreach($products as $product)
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="card bg-dark p-2 mb-4">
                            <div class="card-image">
                                <figure class="image -is-200px-height -overflow-hidden">
                                    <a href="{{ route('product-novelties', ['product' => $product->slug]) }}" class="">
                                        <img src="{{ $product->photo }}" class="img-fluid" alt="{{ $product->photo }}">
                                    </a>
                                </figure>
                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <h2 class="card-title">
                                            {{ $product->name }}
                                        </h2>
                                    </div>
                                </div>
                                <div class="content card-product">
                                    <p>
                                        {!! Str::words($product->description, 10, '...') !!}
                                    </p>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('product-novelties', ['product' => $product->slug]) }}" class="">
                                        Ver más
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>


    </div>
    @include('public.parciales.home.elegirnos')
@endsection
