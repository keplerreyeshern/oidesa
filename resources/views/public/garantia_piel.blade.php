@extends('public.layouts.app')
@section('titulo', 'Garantia Piel Integral')
@section('content')

    <div class="container-fluid -quienes-somos pr-0 p-0">

        <div class="-titulo-area text-right">
            <h1>{{ $data['titulo'] }}</h1>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Garantia Piel Integral</li>
            </ol>
        </nav>



            <div class="container-fluid tm-0">
                <div class="row">
                    <div class="col-3">
                        <div class="-title text-center">
                            <h4><span class="-blanco">Garantia</span><br>Piel Integral</h4>
                        </div>
                    </div>
                    <div class="col-9 col-md-5 offset-md-2" style="margin-top: 50px;">
                        <h1 class="text-center">Organización Industrial de Espumados, S.A. de C.V.</h1>
                        <p class="text-justify">
                            Les garantiza que las piezas inyectadas con poliuretano piel integral, tienen un periodo de garantía por 1 año posterior a la fecha de facturación, cumplen con los estándares internacionales de calidad descritos a continuación:
                        </p>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <h1>PRUEBA</h1>
                                <p>
                                    Durabilidad <br>
                                    Densidad empacada <br>
                                    Dureza <br>
                                    Copression set <br>
                                    Resistencia a la abrasión v Flamabilidad <br>
                                </p>
                            </div>
                            <div class="col-12 col-lg-6">
                                <h1 class="text-center">ESTÁNDAR</h1>
                                <p class="text-center">
                                    3 años en condiciones normales de uso. 344 kg/m3 <br>
                                    75+/-5 SHORE “A” <br>
                                    Menor al 25% <br>
                                    Buena <br>
                                    Cumple la norma MVSS-302 <br>
                                </p>
                            </div>
                        </div>
                        <p class="text-justify">
                            Organización Industrial de Espumados S.A. de C.V. No garantiza los productos descansa brazos, coderas, brazos etc. de piel integral por alguna de las siguientes condiciones:
                        </p>
                        <p class="text-justify">
                            a) Limpieza incorrecta de los productos (Solo se deberá tallar con espuma y un cepillo de cerdas suaves y con un trapo húmedo quitar los residuos)<br>
                            b) Exposición directa al agua. <br>
                            c) Exposición directa a la intemperie. <br>
                            d) Uso incorrecto del usuario final. <br>
                            e) Cualquier evento vandálico. <br>
                            f) Cualquier siniestro natural.
                        </p>
                        <h1 class="text-center">PRECAUCION</h1>
                        <p class="text-justify">
                            Las espumas de poliuretano como cualquier otro material orgánico, son combustibles bajo determinadas circunstancias, el hecho de que en su formulación contengan un agente antiflama cuando así se especifica claramente, no garantiza que sean auto extinguibles, el agente antiflama solo retarda la velocidad de propagación de la misma, pero bajo ninguna circunstancia se recomienda dejar expuesta la espuma de poliuretano, deberá protegerse con
                            algún material que evite se encienda por accidente.
                            NOTA: Esta información está basada sobre resultados de la experiencia y pruebas precisas, pero es proporcionada sin aceptar responsabilidad legal por los daños o perdidas atribuibles a su empleo ya que las condiciones de su uso caen fuera de nuestro control, el propio usuario deberá determinar la adaptabilidad de los productos a sus procesos.
                        </p>
                        <div class=" d-flex justify-content-end">
                            <h4>
                                <a href="{{ asset('files/Garantias/Garantia-Piel-Integral.pdf') }}" target="_blank" class="btn btn-primary">Imprimir</a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

    </div>
    @include('public.parciales.home.elegirnos')
@endsection
