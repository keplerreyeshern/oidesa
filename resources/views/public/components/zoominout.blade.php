<link href="/slider/bannerscollection_zoominout.css" rel="stylesheet" type="text/css">
<link href="/slider/text_classes.css" rel="stylesheet" type="text/css">
<link href="/slider/css3animations.css" rel="stylesheet" type="text/css">

<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>



<script src={{ asset('/js/jquery.touchSwipe.min.js') }} type="text/javascript"></script>
<script src={{ asset('/js/bannerscollection_zoominout.js') }} type="text/javascript"></script>
<!-- must have -->

<script>

    jQuery(function() {
        setTimeout(function(){
            jQuery("#bannerscollection_zoominout_1").bannerscollection_zoominout({
                skin:"opportune",
                width:1920,
                height:600,
                width100Proc:true,
                height100Proc:false,
                responsive:true,
                responsiveRelativeToBrowser:true,
                setAsBg:false,

                autoPlay:12,
                loop:true,

                fadeSlides:true,
                horizontalPosition:"center",
                verticalPosition:"center",
                initialZoom:1,
                finalZoom:1,

                duration:20,
                durationIEfix:30,

                zoomEasing:"ease",
                target:"_blank",
                pauseOnMouseOver:false,

                showAllControllers:true,
                showNavArrows:true,
                showOnInitNavArrows:true,
                autoHideNavArrows:true,
                showBottomNav:true,
                showOnInitBottomNav:true,
                autoHideBottomNav:false,
                showPreviewThumbs:false,
                enableTouchScreen:true,
                absUrl:"",
                scrollSlideDuration:0.8,
                scrollSlideEasing:"easeOutQuad",
                defaultEasing:"swing",
                myloaderTime:1,
                showCircleTimer:true,
                circleRadius:13,
                circleLineWidth:2,
                circleColor:"#ffffff",
                circleAlpha:100,
                behindCircleColor:"##ffffff",
                behindCircleAlpha:20,
                numberOfThumbsPerScreen:0,
                thumbsWrapperMarginTop:-50,
                thumbsOnMarginTop:0,
                defaultExitLeft:0,
                defaultExitTop:0,
                defaultExitFade:1,
                defaultExitDuration:0,
                defaultExitDelay:0,
                defaultExitEasing:"swing",
                defaultExitOFF:true
            });
        }, 500);
    });


</script>
