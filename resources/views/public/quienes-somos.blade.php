quienes-somos.blade.php@extends('public.layouts.app')
@section('titulo', '¿Quienes somos?')
@section('content')

    <div class="container-fluid -quienes-somos pr-0 p-0">

        <div class="-titulo-area text-right">
            <h1>{{ $data['titulo'] }}</h1>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Quienes somos</li>
            </ol>
        </nav>



            <div class="container-fluid tm-0">
                <div class="row">
                    <div class="col-3">
                        <div class="-title text-center">
                            <h4><span class="-blanco">¿Quienes</span><br>Somos?</h4>
                        </div>
                    </div>
                    <div class="col-9 col-md-5 offset-md-2" style="margin-top: 50px;">
                        <h1 class="text-center">Organización Industrial de Espumados, S.A. de C.V.</h1>
                        <p class="text-justify">
                            Iniciando sus actividades en 1987, Ofrece a sus clientes la transformación con alta
                            tecnología y excelente calidad del poliuretano, piel integral, espumas flexibles moldeadas y
                            semi-rígidas, principalmente para la industria automotriz, náutica y mobiliario para oficina,
                            así como productos y diseños especiales.
                        </p>
                        <p class="text-justify">
                            Así mismo contamos con un sistema de gestión de Calidad basado en ISO 9001:2015, lo cual nos
                            permite cumplir con las expectativas de calidad de nuestros clientes
                        </p>
                        <p>Casa certificadora:</p>
                        <div class="row mb-3">
                            <div class="col-4 offset-4">
                                <img src="{{ asset('images/logo_iso.jpg') }}" class="img-flud w-100" alt="iso">
                            </div>
                        </div>
                        <h1 class="text-center">Misión</h1>
                        <p class="text-justify">Crear los mejores productos en poliuretano moldeado para la industria mueblera y automotriz,
                            generando fuentes de empleo, con sentido social utilizando materiales y maquinarias con la
                            mas alta tecnología para el cuidado del medio ambiente.</p>
                        <h1 class="text-center">Visión</h1>
                        <p class="text-justify">OIDESA es una empresa que a través del mejoramiento continuo de nuestros procesos, busca
                            fabricar los mejores productos en base a tecnología y diseño.
                        </p>
                        <p class="text-justify">Buscando ser líderes en producción y venta de productos de poliuretano, brindando satisfacción
                            a los clientes, conservando y fortaleciendo conjuntamente con su personal comprometido con
                            la empresa.
                        </p>
                    </div>
                </div>
            </div>

    </div>
    @include('public.parciales.home.elegirnos')
@endsection
