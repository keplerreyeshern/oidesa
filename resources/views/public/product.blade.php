@extends('public.layouts.app')
@section('titulo', 'Detalle del producto ' . $product->name)
@section('head')
    <meta property="og:url"           content="https://oidesa.com/productos/{{$category->slug}}/{{$product->slug}}" />
    <meta property="og:type"          content="Producto" />
    <meta property="og:title"         content="{{$product->name}}" />
    <meta property="og:description"   content="{{ $product->description }}" />
    <meta property="og:image"         content="https://oidesa.com/{{$product->photo}}" />
@endsection
@section('content')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v9.0&appId=2964023510511757&autoLogAppEvents=1" nonce="0QHTyUn4"></script>

    <div class="container-fluid -quienes-somos pr-0 p-0">

        <div class="-titulo-area text-right">
            <h1>{{ $data['titulo'] }}</h1>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('index') }}">
                        Inicio
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('products-category', ['category' => $category->slug]) }}">
                        {{$category->name}}
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{ $product->name }}</li>
            </ol>
        </nav>



        <div class="container-fluid tm-0">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-5 offset-1 p-3">
                    <div class="col-12">
                        <a data-fancybox="gallery" href="{{ $product->photo}}">
                            <img src="{{ $product->photo }}" class="img-fluid" alt="Imagen">
                        </a>
                    </div>
                    <div class="col-12 mt-3">
                        <div class="row mt-2">
                            @if($category->slug == 'brazos' || $category->slug == 'sillas' || $category->slug == 'sillones' || $category->slug == 'bancas')
                                <div class="col-4 my-lg-2">
                                    <a href="{{ route('full-leather')}}">
                                        <img src="{{ asset('images/garantia-piel-integral.jpg') }}" class="img-fluid" alt="Garantia Piel Integral">
                                    </a>
                                </div>
                            @endif
                            @if($category->slug == 'asientos-automotriz' || $category->slug == 'cascos' || $category->slug == 'respaldos' || $category->slug == 'moldeados-flexibles' || $category->slug == 'sillones')
                                    <div class="col-4 my-lg-2">
                                        <a href="{{ route('polyurethane') }}">
                                            <img src="{{ asset('images/garantia-poliuretano.jpg') }}" class="img-fluid" alt="Garantia-poliuretano">
                                        </a>
                                    </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-center">Galeria</h1>
                        <div class="row mt-2">
                            @foreach($images as $image)
                                <div class="col-4 my-lg-2">
                                    <a data-fancybox="gallery" href="{{ $image->name }}">
                                        <img src="{{ $image->name }}" class="img-fluid" alt="{{ $image->name }}">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-5">
                    <div class="row">
                        <div class="col-12 p-3">
                            <h3>Descripción</h3>
                            <p class="text-white">{!!$product->description == 'null' ? '': $product->description !!}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 p-3">
                            <h3>Detalles</h3>
                            <p class="text-white">{!!$product->details == 'null' ? '': $product->details !!}</p>
                            <h5><strong>Modelo: </strong><span class="text-white">{{ $product->model }}</span></h5>
                            <h5><strong>Clave: </strong><span class="text-white">{{ $product->key }}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 p-3">
                            <h3>Medidas</h3>
                            <h5><strong>Centrimetros: </strong><span class="text-white">{{ $product->measuresCms }}</span></h5>
                            <h5><strong>Pulgadas: </strong><span class="text-white">{{ $product->measuresInches }}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 d-flex justify-content-end">
                            <div class="fb-like" data-href="https://oidesa.com/productos/{{$category->slug}}/{{$product->slug}}"
                                 data-width=""
                                 data-layout="button"
                                 data-action="like"
                                 data-size="small"
                                 data-share="false"
                                 data-color="white"
                            >
                            </div>
                            <iframe class="btnface" src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Foidesa.com%2Fproductos%2F{{ $category->slug }}%2F%2F{{ $product->slug }}&layout=button_count&size=small&appId=2964023510511757&width=99&height=20" width="99" height="35" style="border:none;color:white;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('public.parciales.home.iso9001')
    @include('public.parciales.home.elegirnos')
@endsection
