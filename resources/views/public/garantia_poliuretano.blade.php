@extends('public.layouts.app')
@section('titulo', 'Garantia Poliuretano')
@section('content')

    <div class="container-fluid -quienes-somos pr-0 p-0">

        <div class="-titulo-area text-right">
            <h1>{{ $data['titulo'] }}</h1>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Garantia Poliuretano</li>
            </ol>
        </nav>



            <div class="container-fluid tm-0">
                <div class="row">
                    <div class="col-3">
                        <div class="-title text-center">
                            <h4><span class="-blanco">Garantia</span><br>Poliuretano</h4>
                        </div>
                    </div>
                    <div class="col-9 col-md-5 offset-md-2" style="margin-top: 50px;">
                        <h1 class="text-center">Organización Industrial de Espumados, S.A. de C.V.</h1>
                        <p class="text-justify">
                            Organización Industrial de Espumados S.A. de C.V. garantiza los productos de poliuretano moldeado que fueron vendidos. Por un periodo de 3 años posterior a la fecha de facturación, los cuales cumplen con los estándares internacionales de calidad descritos a continuación:
                        </p>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <h1>PRUEBA</h1>
                                <p>
                                    Compression Set 50% y 75% ü I.F.D.25%,50% Y 65% <br>
                                    Densidad Empacada <br>
                                    Fatiga Dinámica <br>
                                    Resistencia a la Tensión <br>
                                    Resistencia al Desgarre <br>
                                    Resistencia a la Elongación <br>
                                    Flamabilidad (Cumple) <br>
                                </p>
                            </div>
                            <div class="col-12 col-lg-6">
                                <h1 class="text-center">ESTÁNDAR</h1>
                                <p class="text-center">

                                    ASTM D-3574 <br>
                                    ASTM D-3574 <br>
                                    ASTM D-3574 <br>
                                    ASTM D-3574 <br>
                                    ASTM D-3574 <br>
                                    ASTM D-3574 <br>
                                    ASTM D-3574 <br>
                                    MVSS-302 <br>
                                </p>
                            </div>
                        </div>
                        <p class="text-justify">
                            Organización Industrial de Espumados S.A. de C.V. NO garantiza sus productos por alguna de las siguientes condiciones:
                        </p>
                        <p class="text-justify">
                            a) Limpieza incorrecta del Asiento terminado. <br>
                            b) Exposición directa al agua. <br>
                            c) Exposición directa a la intemperie. <br>
                            d) Deberá cubrirse con algún material (Tela, Piel, Vinil etc.) que los proteja de los rayos ultravioleta del sol
                            y abrasión directa por el usuario. <br>
                            e) Manejo incorrecto durante los procesos de ensamblado en el asiento y tapicería. <br>
                            f) Uso incorrecto del usuario final. <br>
                            g) Cualquier evento vandálico. <br>
                            h) Cualquier siniestro natural. <br>
                            i) Cualquier otro estándar que no esté indicado en la presente garantía.
                        </p>
                        <h1 class="text-center">PRECAUCION</h1>
                        <p class="text-justify">
                            Las espumas de poliuretano como cualquier otro material orgánico, son combustibles bajo determinadas circunstancias, el hecho de que en su formulación contengan un agente antiflama cuando así se especifica claramente, no garantiza que sean auto extinguibles, el agente antiflama solo retarda la velocidad de propagación de la misma, pero bajo ninguna circunstancia se recomienda dejar expuesta la espuma de poliuretano, deberá protegerse con
                            algún material que evite se encienda por accidente.
                            NOTA: Esta información está basada sobre resultados de la experiencia y pruebas precisas, pero es proporcionada sin aceptar responsabilidad legal por los daños o perdidas atribuibles a su empleo ya que las condiciones de su uso caen fuera de nuestro control, el propio usuario deberá determinar la adaptabilidad de los productos a sus procesos.
                        </p>
                        <div class=" d-flex justify-content-end">
                            <h4>
                                <a href="{{ asset('files/Garantias/Garantia-Poliuretano.pdf') }}" target="_blank" class="btn btn-primary">Imprimir</a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

    </div>
    @include('public.parciales.home.elegirnos')
@endsection
