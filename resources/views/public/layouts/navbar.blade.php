<nav class="navbar navbar-expand-lg -navbar-oidesa navbar-light -navposition">

    <a class="navbar-brand " href="{{ route('index') }}"><img src="{{ asset('assets/brand/oidesa.png') }}" class="img-fluid -brand"></a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-bars"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">

            <li class="nav-item">
                <a class="nav-link" href="{{ route('novelties') }}">Novedades</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Productos

                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach($categories as $category)
                        @if($category->slug == 'air-mat')
                            <a class="dropdown-item" href="https://airmatflooring.com">{{ $category->name }}</a>
                        @elseif($category->slug == 'nube-foam')
                            <a class="dropdown-item" href="https://airmatflooring.com">{{ $category->name }}</a>
                        @else
                            <a class="dropdown-item" href="{{ route('products-category', ['category' => $category->slug]) }}">{{ $category->name }}</a>
                        @endif
                    @endforeach
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Catálogo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('contact') }}">Contacto</a>
            </li>
        </ul>

    </div>
</nav>
