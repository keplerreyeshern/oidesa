

<footer class=" pt-4">

   <div class="row ">

       <div class="col-md-3 pl-5">
           <h4>Teléfonos</h4>
           <h5>
{{--               <a href="tel:55-5871-9741" class="-tel">55 5871 9741</a>--}}
               <br><a href="tel:55-5871-9742" class="-tel">55 5871 9742</a>
               <br><a href="tel:55-5871-9743" class="-tel">55 5871 9743</a>
               <br><a href="tel:55-5871-9743" class="-tel">55 1079 6762</a>
               <br><a href="tel:55-5871-9743" class="-tel">55 4613 2847</a>
               <br><br>
               Ext. 110 y 111

               <br>
{{--               <br><a href="mailto:gerentecomercial@oidesa.com">gerentecomercial@oidesa.com</a>--}}
{{--               <br><a href="mailto:oidesa@prodigy.net.mx">oidesa@prodigy.net.mx</a>--}}
{{--               <br><a href="mailto:ventasoidesa@prodigy.net.mx">ventasoidesa@prodigy.net.mx</a>--}}
               <br><a href="mailto:ventas@oidesa.com">ventas@oidesa.com</a>
               <br><a href="mailto:ventas1@oidesa.com">ventas1@oidesa.com</a>

           </h5>
       </div>
       <div class="col-md-3 pl-5 pl-md-0">
           <h4>Domicilio</h4>
           <h5>
               Pirules 16E Int 1
               <br>Complejo Industrial Cuamatla
               <br>Cuahutitlán Izcalli, 54730
               <br>Estado de México
           </h5>
           <br><br>
           <h4>
               <a href="{{ route('about') }}" class="btn btn-primary">¿Quienes Somos?</a>
           </h4>
       </div>
       <div class="col-md-6 pb-4 pr-5 pl-5 pl-md-0">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3757.130221476206!2d-99.20917488508962!3d19.66444898675304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d21e335e8358d5%3A0x7382221ef62c5d77!2sOrganizacion%20Industrial%20de%20Espumados%20SA%20de%20CV%20(OIDESA)!5e0!3m2!1ses-419!2smx!4v1624507011123!5m2!1ses-419!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
       </div>


   </div>

    <div class="row -copy">
        <div class="col text-center">
            <p>Copyright |
                <a href="{{ route('privacy') }}">Aviso de Privacidad</a>
            </p>
        </div>
    </div>

</footer>








