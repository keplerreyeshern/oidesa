@if($productsNews)
    <div class="container-fluid -productos-nuevos">
        <div class="row d-flex justify-content-center">
            <div class="-titulo text-center">
                <h4><span class="-blanco">Productos</span> Nuevos</h4>
            </div>
        </div>
        <br>
        <div class="col-12 text-center">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($productsNews as $key => $product)
                        <div class="carousel-item  -producto {{ $key == 0 ? 'active':'' }}">
                            <div class="col-7 mx-auto">
                                <div class="card">
                                    <div class="card-horizontal">
                                        <div class="">
                                            <img class="img-fluid" src="{{ $product->photo }}" alt="">
                                        </div>
                                        <div class="card-body text-left">
                                            <h4 class="card-title">{!! $product->name !!}</h4>
                                            <p class="card-text">{!! $product->description !!}</p>

                                            <a href="{{ route('product', ['category' => $product->categories[0]->slug, 'product' => $product->slug]) }}" class="btn btn-primary">Mas información</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
@endif
