<div class="container-fluid  -elegirnos">

    <br>
    <div class="row pt-5 pb-5">

        <div class="col-md-3">
            <div class="-titulo text-center">
                <h4><span class="-blanco">ISO</span><br>9001</h4>
            </div>
        </div>

        <div class="col-md-1 offset-md-1 mt-3 mt-md-0 d-flex justify-content-center">
            <div class="card">
                <a href="{{ route('about') }}">
                    <img src="{{ asset('images/logo_iso.jpg') }}" class="img-fluid w-100" alt="iso">
                </a>
            </div>
        </div>

        <div class="col-md-4 offset-md-2 d-flex justify-content-center mt-3 mt-md-0">
            <div class="card">
                <p class="card-text text-center">Contamos con un sistema de gestión de Calidad basado en ISO 9001:2015, lo cual nos permite cumplir con las expectativas de calidad de nuestros clientes </p>
            </div>
        </div>

    </div>
    <br>
</div>
