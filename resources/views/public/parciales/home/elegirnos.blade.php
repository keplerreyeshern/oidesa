<div class="container-fluid  -elegirnos">

    <br>
    <div class="row pt-5 pb-5">

        <div class="col-md-3 mb-5 mb-md-0">
            <div class="-titulo text-center">
                <h4><span class="-blanco">¿Por que</span><br>elegirnos?</h4>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <span class="h1 text-center"><i class="fas fa-bahai -ico-titulo"></i></span>
                <div class="card-body">
                    <h5 class="card-title text-center">Nosotros</h5>
                    <div class="row card-text" style="background-image: null !important; background: transparent !important;">
                        <div class="col-12">
                            <br><br>
                            <p class="card-ext text-center">Somos una empresa con más de 30 años de experiencia en la transformación de poliuretano con tecnología de punta.</p>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card ">
                <span class="h1 text-center"><i class="fab fa-black-tie -ico-titulo"></i></span>
                <div class="card-body">
                    <h5 class="card-title text-center">Calidad</h5>
                    <div class="row card-text" style="background-image: null !important; background: transparent !important;">
                        <div class="col-4 mimg">
                            <a href="{{ route('about') }}">
                                <img src="{{ asset('images/logo_iso.jpg') }}" class="img-fluid w-100" alt="iso">
                            </a>
                        </div>
                        <div class="col-8">
                            <p class="card-tet text-center">Contamos con sistemas de gestión de calidad ISO 9001 : 2015 para clientes que lo requieran.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <span class="h1 text-center"><i class="far fa-eye -ico-titulo"></i></span>
                <div class="card-body">
                    <h5 class="card-title text-center">Servicio</h5>
                    <div class="row card-text" style="background-image: null !important; background: transparent !important;">
                        <div class="col-12">
                            <br><br>
                            <p class="card-txt text-center">Ofrecemos la fabricación de proyectos, de acuerdo a sus necesidades.</p>
                            <br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</div>
