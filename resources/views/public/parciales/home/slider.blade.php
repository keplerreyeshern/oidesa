<div id="bannerscollection_zoominout_1">
    <div class="myloader"></div>

    <ul class="bannerscollection_zoominout_list">
        <li data-text-id="#bannerscollection_zoominout_photoText78" data-video="false" data-bottom-thumb="/slider/images/oportuneFullWidthLayer/thumbs/0t_opportune.jpg" data-link="" data-target=""  data-horizontalPosition="" data-verticalPosition="" data-initialZoom="1" data-finalZoom="1" data-duration="" data-zoomEasing="" ><img src="/slider/images/oportuneFullWidthLayer/paths.jpg" width="1920" height="600" style="width:1920px; height:600px;" alt="" /></li>
        <li data-text-id="#bannerscollection_zoominout_photoText110" data-video="false" data-bottom-thumb="/slider/images/oportuneFullWidthLayer/thumbs/01t_opportune.jpg" data-link="" data-target="" data-autoPlay="17" data-horizontalPosition="" data-verticalPosition="" data-initialZoom="" data-finalZoom="" data-duration="" data-zoomEasing="" ><img src="/slider/images/oportuneFullWidthLayer/castle_big.jpg" width="1920" height="600" style="width:1920px; height:600px;" alt="" /></li>
        <li data-text-id="#bannerscollection_zoominout_photoText76" data-video="false" data-bottom-thumb="/slider/images/oportuneFullWidthLayer/thumbs/03t_opportune.jpg" data-link="" data-target=""  data-horizontalPosition="center" data-verticalPosition="bottom" data-initialZoom="1" data-finalZoom="1" data-duration="" data-zoomEasing="ease" ><img src="/slider/images/oportuneFullWidthLayer/bulbbg.jpg" width="1920" height="600" style="width:1920px; height:600px;" alt="" /></li>
    </ul>



    <div id="bannerscollection_zoominout_photoText78" class="bannerscollection_zoominout_texts">
        <div class="bannerscollection_zoominout_text_line lbg1_ImageOnly" data-initial-left="641" data-initial-top="-29" data-final-left="641" data-final-top="-29" data-duration="1" data-fade-start="0" data-delay="0.9" data-easing="swing" data-css3-animation="planeFW"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/blue.png" width="27" height="26" style="width:27px; height:26px; border:0;" alt="blue" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ImageOnly" data-initial-left="1717" data-initial-top="-77" data-final-left="1717" data-final-top="-77" data-duration="1" data-fade-start="0" data-delay="1" data-easing="swing" data-css3-animation="planeBW"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/blue.png" width="27" height="26" style="width:27px; height:26px; border:0;" alt="blue2" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ImageOnly" data-initial-left="395" data-initial-top="284" data-final-left="395" data-final-top="284" data-duration="1" data-fade-start="0" data-delay="0.7" data-easing="swing" data-css3-animation="planeFW"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/white_.png" width="27" height="26" style="width:27px; height:26px; border:0;" alt="white" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ImageOnly" data-initial-left="1635" data-initial-top="480" data-final-left="1635" data-final-top="480" data-duration="0.5" data-fade-start="0" data-delay="0" data-easing="swing" data-css3-animation="planeBW"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/white_.png" width="27" height="26" style="width:27px; height:26px; border:0;" alt="white2" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ImageOnly" data-initial-left="27" data-initial-top="583" data-final-left="27" data-final-top="583" data-duration="1" data-fade-start="0" data-delay="1.2" data-easing="swing" data-css3-animation="planeBW"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/yellow_.png" width="32" height="32" style="width:32px; height:32px; border:0;" alt="yellow" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ImageOnly" data-initial-left="1431" data-initial-top="128" data-final-left="1431" data-final-top="128" data-duration="1" data-fade-start="0" data-delay="1.4" data-easing="swing" data-css3-animation="planeFW"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/yellow_.png" width="32" height="32" style="width:32px; height:32px; border:0;" alt="yellow2" /></div>
        </div>

        <div class="bannerscollection_zoominout_text_line lbg1_TinyRegular" data-initial-left="649" data-initial-top="-82" data-final-left="620" data-final-top="-82" data-duration="0.5" data-fade-start="0" data-delay="3.5" data-easing="swing" data-css3-animation="flipInX"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/whitebig.png" width="877" height="779" style="width:877px; height:779px; border:0;" alt="whiteBig" /></div>
        </div>

        <div class="bannerscollection_zoominout_text_line lbg1_UltraLargeGrand" data-initial-left="850" data-initial-top="240" data-final-left="850" data-final-top="240" data-duration="0.5" data-fade-start="0" data-delay="4.5" data-easing="swing" data-css3-animation="slideUp"   data-intermediate-delay="0">
            <div class="lbg_inner_div -exo" style="color:#005ea6;border-width:0px;border-style:solid;">O</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_UltraLargeGrand" data-initial-left="1026" data-initial-top="240" data-final-left="932" data-final-top="240" data-duration="0.5" data-fade-start="0" data-delay="4.7" data-easing="swing" data-css3-animation="slideDown"   data-intermediate-delay="0">
            <div class="lbg_inner_div -exo" style="color:#005ea6;border-width:0px;border-style:solid;">I</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_UltraLargeGrand" data-initial-left="1111" data-initial-top="240" data-final-left="964" data-final-top="240" data-duration="0.5" data-fade-start="0" data-delay="4.9" data-easing="swing" data-css3-animation="slideUp"   data-intermediate-delay="0">
            <div class="lbg_inner_div -exo" style="color:#005ea6;border-width:0px;border-style:solid;">D</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_UltraLargeGrand" data-initial-left="1097" data-initial-top="240" data-final-left="1045" data-final-top="240" data-duration="0.5" data-fade-start="0" data-delay="4.9" data-easing="swing" data-css3-animation="slideDown"   data-intermediate-delay="0">
            <div class="lbg_inner_div -exo" style="color:#005ea6;border-width:0px;border-style:solid;">E</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_UltraLargeGrand" data-initial-left="1097" data-initial-top="240" data-final-left="1120" data-final-top="240" data-duration="0.5" data-fade-start="0" data-delay="4.9" data-easing="swing" data-css3-animation="slideUp"   data-intermediate-delay="0">
            <div class="lbg_inner_div -exo" style="color:#005ea6;border-width:0px;border-style:solid;">S</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_UltraLargeGrand" data-initial-left="1097" data-initial-top="240" data-final-left="1190" data-final-top="240" data-duration="0.5" data-fade-start="0" data-delay="4.9" data-easing="swing" data-css3-animation="slideDown"   data-intermediate-delay="0">
            <div class="lbg_inner_div -exo" style="color:#005ea6;border-width:0px;border-style:solid;">A</div>
        </div>


        <div class="bannerscollection_zoominout_text_line lbg1_MediumGrand" data-initial-left="1022" data-initial-top="436" data-final-left="950" data-final-top="436" data-duration="0.5" data-fade-start="0" data-delay="7" data-easing="swing" data-css3-animation="scaleOutBounce"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#005ea6;border-width:0px;border-style:solid;">para su empresa!</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_MediumGrand" data-initial-left="945" data-initial-top="386" data-final-left="920" data-final-top="386" data-duration="0.5" data-fade-start="0" data-delay="6.5" data-easing="swing" data-css3-animation="rollInLeft"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#005ea6;border-width:0px;border-style:solid;">Productos pensados</div>
        </div>



        <div class="bannerscollection_zoominout_text_line lbg1_TinyRegular" data-initial-left="284" data-initial-top="390" data-final-left="284" data-final-top="390" data-duration="0.5" data-fade-start="0" data-delay="8" data-easing="swing" data-css3-animation=""   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/whitesmallbottom.png" width="401" height="356" style="width:401px; height:356px; border:0;" alt="whiteSmall" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_TinyRegular" data-initial-left="425" data-initial-top="-170" data-final-left="425" data-final-top="-170" data-duration="0.5" data-fade-start="0" data-delay="7.5" data-easing="swing" data-css3-animation=""   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/whitesmalltop.png" width="401" height="356" style="width:401px; height:356px; border:0;" alt="whiteSmall" /></div>
        </div>

        <div class="bannerscollection_zoominout_text_line lbg1_TinyRegular" data-initial-left="1314" data-initial-top="-168" data-final-left="1214" data-final-top="-68" data-duration="0.5" data-fade-start="0" data-delay="8" data-easing="swing" data-css3-animation="twisterInDown"   data-intermediate-delay="1" data-intermediate-left="893" data-intermediate-top="270" data-intermediate-duration="1" data-intermediate-easing="swing">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/magnifying6.png" width="462" height="477" style="width:462px; height:477px; border:0;" alt="IMAGE 18" /></div>
        </div>
    </div>





    <div id="bannerscollection_zoominout_photoText110" class="bannerscollection_zoominout_texts">
        <div class="bannerscollection_zoominout_text_line transp10" data-initial-left="0" data-initial-top="0" data-final-left="0" data-final-top="0" data-duration="3" data-fade-start="0" data-delay="0" data-easing="swing" data-css3-animation=""   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/skins/empty50.png" width="50" height="50" style="width:50px; height:50px; border:0;" alt="transparency10" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ImageOnly" data-initial-left="698" data-initial-top="86" data-final-left="698" data-final-top="86" data-duration="1" data-fade-start="0" data-delay="3" data-easing="swing" data-css3-animation=""   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:10px;border-style:solid;border-color:#ffffff;"><img src="/slider/images/oportuneFullWidthLayer/middlebg.jpg" width="501" height="417" style="width:501px; height:417px; border:0;" alt="Middle" /></div>
        </div>

        <div class="bannerscollection_zoominout_text_line lbg1_SmallGrand" data-initial-left="757" data-initial-top="434" data-final-left="730" data-final-top="434" data-duration="0" data-fade-start="0" data-delay="6" data-easing="swing" data-css3-animation=""   data-intermediate-delay="5.8" data-intermediate-css3-animation="dance">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:0px;border-style:solid;">terminados, inyectados &#038; espumados</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_MediumGrand" data-initial-left="764" data-initial-top="390" data-final-left="840" data-final-top="390" data-duration="0" data-fade-start="0" data-delay="6" data-easing="swing" data-css3-animation=""   data-intermediate-delay="4.6" data-intermediate-css3-animation="flash">
            <div class="lbg_inner_div" style="color:#d4874f;border-width:0px;border-style:solid;">A SU MEDIDA</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ExtraLargeGrand" data-initial-left="835" data-initial-top="258" data-final-left="835" data-final-top="258" data-duration="0" data-fade-start="0" data-delay="6" data-easing="swing" data-css3-animation=""   data-intermediate-delay="3.7" data-intermediate-css3-animation="magnifyingBounce">
            <div class="lbg_inner_div -exo" style="color:#ffffff;border-width:0px;border-style:solid;">TOTAL</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ExtraLargeGrand" data-initial-left="735" data-initial-top="175" data-final-left="765" data-final-top="175" data-duration="0" data-fade-start="0" data-delay="6" data-easing="swing" data-css3-animation=""   data-intermediate-delay="1.5" data-intermediate-css3-animation="flipYFastRight3d">
            <div class="lbg_inner_div -exo" style="color:#ffffff;border-width:0px;border-style:solid;">CALIDAD</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_SmallGrand" data-initial-left="828" data-initial-top="133" data-final-left="850" data-final-top="133" data-duration="0" data-fade-start="0" data-delay="6" data-easing="swing" data-css3-animation=""   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#d4874f;border-width:0px;border-style:solid;">Atención única</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_TinyGrand" data-initial-left="749" data-initial-top="354" data-final-left="820" data-final-top="354" data-duration="0" data-fade-start="0" data-delay="6" data-easing="swing" data-css3-animation=""   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#d4874f;border-width:0px;border-style:solid;">Fabricación de modelos</div>
        </div>
        <div class="bannerscollection_zoominout_text_line front" data-initial-left="698" data-initial-top="86" data-final-left="698" data-final-top="86" data-duration="1" data-fade-start="0" data-delay="3" data-easing="swing" data-css3-animation="flipBookRight_Cover4"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:10px;border-style:solid;border-color:#ffffff;"><img src="/slider/images/oportuneFullWidthLayer/folding_c4.jpg" width="501" height="417" style="width:501px; height:417px; border:0;" alt="Cover4" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line back" data-initial-left="698" data-initial-top="86" data-final-left="698" data-final-top="86" data-duration="1" data-fade-start="0" data-delay="3" data-easing="swing" data-css3-animation="flipBookRight_Cover3"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:10px;border-style:solid;border-color:#ffffff;"><img src="/slider/images/airmat.jpg" width="501" height="417" style="width:501px; height:417px; border:0;" alt="Cover3" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line back" data-initial-left="698" data-initial-top="86" data-final-left="698" data-final-top="86" data-duration="1" data-fade-start="0" data-delay="3" data-easing="swing" data-css3-animation="flipBookLeft_Cover2"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:10px;border-style:solid;border-color:#ffffff;"><img src="images/NUBEfoam.jpeg" width="501" height="417" style="width:501px; height:417px; border:0;" alt="Cover2" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line front" data-initial-left="698" data-initial-top="86" data-final-left="698" data-final-top="86" data-duration="0" data-fade-start="0" data-delay="0.5" data-easing="swing" data-css3-animation="journal"   data-intermediate-delay="2" data-intermediate-css3-animation="flipBookLeft_Cover1">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:10px;border-style:solid;border-color:#ffffff;"><img src="/slider/images/oportuneFullWidthLayer/folding_c1.jpg" width="501" height="417" style="width:501px; height:417px; border:0;" alt="Cover1" /></div>
        </div>
    </div>



    <div id="bannerscollection_zoominout_photoText76" class="bannerscollection_zoominout_texts">
        <div class="bannerscollection_zoominout_text_line lbg1_TinyRegular" data-initial-left="890" data-initial-top="0" data-final-left="890" data-final-top="0" data-duration="0.5" data-fade-start="1" data-delay="0.5" data-easing="swing" data-css3-animation="expandUp"   data-exit-left="0" data-exit-top="0" data-exit-fade="0" data-exit-duration="0.5" data-exit-delay="0.1" data-exit-easing="swing" data-exit-off="false"  data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#ffffff;border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/head.png" width="866" height="600" style="width:866px; height:600px; border:0;" alt="head" /></div>
        </div>

        <div class="bannerscollection_zoominout_text_line lbg1_ExtraLargeGrand" data-initial-left="129" data-initial-top="125" data-final-left="129" data-final-top="125" data-duration="1" data-fade-start="0" data-delay="3.5" data-easing="easeInQuad" data-css3-animation="openDownLeftRetourn"   data-intermediate-delay="0">
            <div class="lbg_inner_div -exo" style="color:#ffffff;border-width:0px;border-style:solid;">LA IMAGINACIÓN</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ExtraLargeGrand" data-initial-left="129" data-initial-top="208" data-final-left="129" data-final-top="208" data-duration="1" data-fade-start="0" data-delay="3.5" data-easing="easeInQuad" data-css3-animation="openUpLeftRetourn"   data-intermediate-delay="0">
            <div class="lbg_inner_div -exo" style="color:#ffffff;border-width:0px;border-style:solid;">NO TIENE LIMITES</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_ImageOnly" data-initial-left="138" data-initial-top="315" data-final-left="138" data-final-top="315" data-duration="0" data-fade-start="0" data-delay="6" data-easing="swing" data-css3-animation="coolBarLeft"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="color:#ffffff;background-color:#ffffff;border-width:0px;border-style:solid;"></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_TinyRegular" data-initial-left="1158" data-initial-top="135" data-final-left="1158" data-final-top="135" data-duration="0.6" data-fade-start="0" data-delay="1" data-easing="swing" data-css3-animation="jello"   data-intermediate-delay="0.8" data-intermediate-css3-animation="rotateEaseForward">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/w_001.png" width="157" height="157" style="width:157px; height:157px; border:0;" alt="wheel1" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_TinyRegular" data-initial-left="1292" data-initial-top="214" data-final-left="1292" data-final-top="214" data-duration="0.6" data-fade-start="0" data-delay="1.3" data-easing="swing" data-css3-animation="jello"   data-intermediate-delay="1.1" data-intermediate-css3-animation="rotateEaseBackward">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/w_002.png" width="125" height="125" style="width:125px; height:125px; border:0;" alt="wheel2" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_TinyRegular" data-initial-left="1357" data-initial-top="130" data-final-left="1357" data-final-top="130" data-duration="0.5" data-fade-start="0" data-delay="1.5" data-easing="swing" data-css3-animation="boingInUp"   data-intermediate-delay="1" data-intermediate-css3-animation="rotateEaseForward">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/w_003.png" width="106" height="106" style="width:106px; height:106px; border:0;" alt="wheel3" /></div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_SmallGrand" data-initial-left="137" data-initial-top="334" data-final-left="137" data-final-top="334" data-duration="0.5" data-fade-start="0" data-delay="5.5" data-easing="swing" data-css3-animation="boingInUp"   data-intermediate-delay="2.6" data-intermediate-css3-animation="jello">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;">Spinning passionately the little wheels of <br /> phantasy to find the road to succes. Don&#8217;t give up! <br />Put an extra wheel if necessary.</div>
        </div>
        <div class="bannerscollection_zoominout_text_line lbg1_TinyRegular" data-initial-left="890" data-initial-top="0" data-final-left="890" data-final-top="0" data-duration="0.7" data-fade-start="0" data-delay="7.5" data-easing="swing" data-css3-animation="flash"   data-intermediate-delay="0">
            <div class="lbg_inner_div" style="border-width:0px;border-style:solid;"><img src="/slider/images/oportuneFullWidthLayer/headbulb.png" width="866" height="600" style="width:866px; height:600px; border:0;" alt="heabulb" /></div>
        </div>
    </div>
</div>
