
<div class="container-fluid  -lineas ">


    <div  class="row">
        <div class="col-lg-3 ">

            <div class="-titulo text-center">
                <h4>Lineas</h4>
            </div>
            <div class="-intro pt-4 pr-1 pl-1">
                <h4 class="-lines text-center">
                    @foreach($categories as $category)
                        @if($category->slug == 'air-mat')
                            <a href="https://airmatflooring.com">
                                <span>{{ $category->name }}</span> <br>
                            </a>
                        @elseif($category->slug == 'nube-foam')
                            <a href="https://airmatflooring.com">
                                <span>{{ $category->name }}</span> <br>
                            </a>
                        @else
                            <a href="{{ route('products-category', ['category' => $category->slug]) }}">
                                <span>{{ $category->name }}</span> <br>
                            </a>
                        @endif
                    @endforeach
                </h4>
{{--                <div class="text-center">--}}
{{--                    <a href="/catalogos" class="btn btn-primary ">Catalogos</a>--}}
{{--                </div>--}}
                <p class="text-center text-lg-left">Nuestra experiencia en poliuretano moldeado nos permite tener diferentes líneas de productos</p>
            </div>

        </div>

        <div class="col-lg-9 mt-5 mt-lg-0">
            <div class="row">


                <div class="col-md-4">

                    <div class="flip-card" id="mol">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="{{ asset('images/lines/moldeados-flexibles.jpg') }}" class="img-lineas w-100">
                            </div>
                            <div class="flip-card-back">
                                <div class="m-3 -cont">
                                    <a href="{{ route('products-category', ['category' => 'moldeados-flexibles']) }}">
                                        <h2>Moldeados Flexibles</h2>
                                    </a>
                                    <p>La mas alta calidad apegados  a los lineamientos de ISO 9001; 2015</p>
                                    <a href="{{ route('products-category', ['category' => 'moldeados-flexibles']) }}" class="btn btn-primary">Ver todos</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="{{ asset('images/lines/sillas.jpg') }}" class="img-lineas w-100">
                            </div>
                            <div class="flip-card-back">
                                <div class="m-3 -cont">
                                    <a href="{{ route('products-category', ['category' => 'sillas']) }}">
                                        <h2>Sillas</h2>
                                    </a>
                                    <p>Ergonomía total con los mejores componentes y diseño de vanguardia.</p>
                                    <a href="{{ route('products-category', ['category' => 'sillas']) }}" class="btn btn-primary">Ver todos</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="{{ asset('images/lines/sillones.jpg') }}" class="img-lineas w-100">
                            </div>
                            <div class="flip-card-back">
                                <div class="m-3 -cont">
                                    <a href="{{ route('products-category', ['category' => 'sillones']) }}">
                                        <h2>Sillones</h2>
                                    </a>
                                    <p>Comodidad para sus clientes en toda sala de espera, su imagen correctamente proyectada a través del confort y el diseño.</p>
                                    <a href="{{ route('products-category', ['category' => 'sillones']) }}" class="btn btn-primary">Ver todos</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-md-4">

                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="{{ asset('images/lines/air-mat.jpg') }}" class="img-lineas w-100">
                            </div>
                            <div class="flip-card-back">
                                <div class="m-3 -cont">
                                    <a href="https://airmatflooring.com" target="_blank">
                                        <h2>Air Mart</h2>
                                    </a>
                                    <p>La opción ideal para su negocio, confort y seguridad para su persona en área de trabajo comerciales y/o industriales.</p>
                                    <a href="https://airmatflooring.com" target="_blank" class="btn btn-primary">Ver todos</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>




                <div class="col-md-4">

                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="{{ asset('images/lines/bancas.jpg') }}" class="img-lineas w-100">
                            </div>
                            <div class="flip-card-back">
                                <div class="m-3 -cont">
                                    <a href="{{ route('products-category', ['category' => 'bancas']) }}">
                                        <h2>Bancas</h2>
                                    </a>
                                    <p>Proyecte su imagen con los mejores diseños y la mayor comodidad para sus clientes.</p>
                                    <a href="{{ route('products-category', ['category' => 'bancas']) }}" class="btn btn-primary">Ver todos</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="{{ asset('images/lines/brazos.jpg') }}" class="img-lineas w-100">
                            </div>
                            <div class="flip-card-back">
                                <div class="m-3 -cont">
                                    <a href="{{ route('products-category', ['category' => 'brazos']) }}">
                                        <h2>Brazos</h2>
                                    </a>
                                    <p>Los mejores elementos para integrar sus propios diseños con piezas de la mas alta calidad.</p>
                                    <a href="{{ route('products-category', ['category' => 'brazos']) }}" class="btn btn-primary">Ver todos</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">

                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="{{ asset('images/lines/nubefoam.jpg') }}" class="img-lineas w-100">
                            </div>
                            <div class="flip-card-back">
                                <div class="m-3 -cont">
                                    <a href="https://airmatflooring.com">
                                        <h2>Nube Foam</h2>
                                    </a>
                                    <p>El mejor concepto para su descanso.</p>
                                    <a href="https://airmatflooring.com" target="_blank" class="btn btn-primary">Ver todos</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-fro">
                                <img src="{{ asset('images/lines/oidesa.jpg') }}" class="img-lineas w-100">
                            </div>
                            <div class="flip-card-ba">
                                <div class="m-3 -cont">
                                    <a >
                                        <h2></h2>
                                    </a>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-fro">
                                <img src="{{ asset('images/lines/oidesa.jpg') }}" class="img-lineas w-100">
                            </div>
                            <div class="flip-card-ba">
                                <div class="m-3 -cont">
                                    <a >
                                        <h2></h2>
                                    </a>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        $(window).ready(function (){
            var molv = false;
            var linkmol = $("#linkmol");
            var cradmol = $("#mol");
            linkmol.hover(function (){
                if (molv){
                    cradmol.addClass('flip-card-front');
                } else {
                    cradmol.removeClass('flip-card-front');
                }
            });
        });
    </script>


</div>
