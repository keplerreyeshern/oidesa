@extends('public.layouts.app')
@section('titulo', 'Inicio')
@section('content')


    @include('public.parciales.home.slider')

    @include('public.parciales.home.productos-nuevos')

    @include('public.parciales.home.elegirnos')

    @include('public.parciales.home.lineas')

    @include('public.parciales.home.iso9001')


@endsection
