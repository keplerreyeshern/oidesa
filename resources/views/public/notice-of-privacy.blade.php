@extends('public.layouts.app')
@section('titulo', 'Aviso de privacidad')
@section('content')

    <div class="container-fluid -quienes-somos pr-0 p-0">

        <div class="-titulo-area text-right">
            <h1>{{ $data['titulo'] }}</h1>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Aviso de Privacidad</li>
            </ol>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-3">
                <span>
                    <i class="fa fa-user-shield fa-10x"></i>
                </span>
                </div>
                <div class="col-12 col-sm-9">
                    <h4 class="title is-4">
                        Aviso dirigido a los titulares de datos personales que obran en posesión de {{ config('app.name') }}
                    </h4>

                    <p>
                        Organización Industrial de Espumados S.A. de C.V., con domicilio en Complejo industrial cuamatla, Cuautitlan Izcalli, Estado de México
                        C.P. 54730. hace de su conocimiento que los datos personales de usted, incluyendo los sensibles,
                        que actualmente o en el futuro obren en nuestras bases de datos, serán tratados y/o utilizados por:
                        Organización Industrial de Espumados S.A. de C.V. y/o las empresas controladoras de ésta última y/o nuestras empresas filiales y/o
                        subsidiarias y/o aquellos terceros que, por la naturaleza de sus trabajos o funciones tengan la necesidad
                        de tratar y/o utilizar sus datos personales; con el propósito de cumplir aquellas obligaciones que se
                        derivan de la relación jurídica existente entre usted como titular de los datos personales y las empresas
                        antes señaladas.
                    </p><br>
                    <p>
                        Organización Industrial de Espumados S.A. de C.V. se reserva el derecho de cambiar, modificar, complementar y/o alterar el presente
                        aviso, en cualquier momento, en cuyo caso se hará de su conocimiento a través de cualquiera de los medios
                        que establece la legislación en la materia.
                    </p> <br>
                    <h4 class="title is-4">
                        Aviso sobre el uso de cookies por parte de Organización Industrial de Espumados S.A. de C.V.:
                    </h4>
                    <p>
                        En oidesa.com se implementa el uso de "cookies" y otras tecnologías de publicidad para recopilar datos y
                        direcciones IP; gracias a ellas podemos entender mejor el comportamiento de nuestros visitantes y
                        ofrecerles contenido personalizado, de acuerdo a los servicios y soluciones que brinda Organización Industrial de Espumados S.A. de C.V..
                        Es importante señalar que, de ninguna manera, con el uso de "cookies" se extrae información de su computadora  que pudiera vulnerar a
                        nuestros clientes y visitantes.  En el caso de direcciones IP e información personal  se sigue el protocolo vigente de uso y
                        protección de datos manejado por {{ config('app.name') }}.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
