@extends('errors::illustrated-layout')

@section('code', '404 😭')

@section('title', trans('errors.404.title'))

@section('image')

    <div style="background-image: url({{ asset('/images/404-bg.png') }});" class="absolute pin bg-no-repeat md:bg-left lg:bg-center">
    </div>

@endsection

@section('message', trans('errors.404.message'))
@section('button', trans('errors.404.button'))
