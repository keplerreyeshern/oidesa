<?php

return [
    '404' => [
        'title' => 'Página no encontrada',
        'message' => 'Lo sentimos, no se pudo encontrar la página que estás buscando.',
        'button' => 'Ir al inicio'
    ]
];
