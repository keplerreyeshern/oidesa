<?php

return [
    '404' => [
        'title' => 'Page Not Found',
        'message' => 'Sorry, the page you are looking for could not be found.',
        'button' => 'go Home'
    ]
];
