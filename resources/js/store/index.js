import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        addVaS: false
    },
    mutations: {
        eventadd(state, value){
            state.addVaS = value;
        },
    }
});
