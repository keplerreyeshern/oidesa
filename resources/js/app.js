/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./bannerscollection_zoominout');
require('./jquery.touchSwipe.min');

window.Vue = require('vue');
import VuePaginate from 'vue-paginate';
import vue2Dropzone from 'vue2-dropzone';
import store from './store';

Vue.use(VuePaginate);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('categories-component', require('./components/CategoriesComponent.vue').default);
Vue.component('sub-categories-component', require('./components/SubCategoriesComponent.vue').default);
Vue.component('products-component', require('./components/ProductsComponent.vue').default);
Vue.component('product-create-component', require('./components/ProductCreateComponent.vue').default);
Vue.component('product-sub-category-component', require('./components/ProductSubCategoryComponent.vue').default);
Vue.component('galleries-component', require('./components/GalleriesComponent.vue').default);
Vue.component('image-gallery-component', require('./components/ImageGalleryComponent.vue').default);
Vue.component('release-cred-component', require('./components/ReleaseCREDComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});
