<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use SoftDeletes, HasTranslations;

    protected $table = "categories";

    public $fillable = ['name', 'slug', 'parent_id', 'type'];

    public $translatable = ['name'];

    public function childs(){
        return $this->hasMany('App\Category','parent_id', 'id');
    }

    /**
     * The users that belong to the product.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
