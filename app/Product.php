<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{
    use Sluggable, HasTranslations, SoftDeletes;

    protected $table = "products";


    public $translatable = [
        'name',
        'description',
        'details',
    ];

    protected $fillable = [
        'name',
        'slug',
        'new',
        'key',
        'model',
        'full_leather',
        'polyurethane',
        'description',
        'details',
        'measuresCms',
        'measuresInches',
        'photo',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function categories(){
        return $this->belongsToMany('App\Category')->withTimestamps();
    }
}
