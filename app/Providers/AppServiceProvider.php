<?php

namespace App\Providers;

use App\Category;
use App\Product;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('public/layouts/navbar', function($view) {
            $categories = Category::where('type', 'products')->whereNull('parent_id')->get();
            $view->with('categories', $categories);
        });

        view()->composer('public/parciales/home/lineas', function($view) {
            $categories = Category::where('type', 'products')->whereNull('parent_id')->get();
            $view->with('categories', $categories);
        });

        view()->composer('public/parciales/home/productos-nuevos', function($view) {
            $productsNews = Product::where('new', true)->get();
            $view->with('productsNews', $productsNews);
        });
    }
}
