<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImageGallery extends Model
{
    use SoftDeletes;

    protected $table = "images";

    protected $fillable = ['name', 'user_id', 'gallery_id', 'deleted_at', 'active', 'type', 'reference'];
}
