<?php

namespace App\Http\Controllers\cp;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['titulo'] = 'Usuarios';
        $data['modulo'] = 'Usuarios';

        //lista de todos los usuarios
        $users = User::orderBy('name', 'asc')->get();

        return view('cp.modulos.usuarios.index', compact('data','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['titulo'] = 'Usuarios';
        $data['modulo'] = 'Usuarios';

        return view('cp.modulos.usuarios.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' =>'required|string|max:255',
            'email' =>'required|string|email|max:255|unique:users',
            'password' =>'required|string|min:8|confirmed',
        ], [
            'name.required' =>'El campo nombre es obligatorio',
            'name.string' =>'El campo nombre debe ser texto',
            'name.max' =>'El campo nombre debe tener como maximo 255 caracteres',
            'email.required' =>'El campo email es obligatorio',
            'email.max' =>'El campo email debe tener como maximo 255 caracteres',
            'email.unique' =>'El campo email ya se encuentra en la base de datos',
            'email.email' =>'el campo email debe tener un formato valido',
            'password.required' =>'El campo contraseña es obligatorio',
            'password.string' =>'El campo contraseña debe ser texto',
            'password.min' =>'El campo contraseña debe tener como minimo 8 caracteres',
            'password.confirmed' =>'Las contraseñas deben coincidir',
        ]);
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return redirect('/cp/users')->with('success', 'El usuario se creo correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['titulo'] = 'Usuarios';
        $data['modulo'] = 'Usuarios';

        $user = User::find($id);

        return view('cp.modulos.usuarios.edit', compact('data', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $data = $request->validate([
            'name' =>'required|string|max:255',
            'email' =>'required|string|email|max:255|unique:users,id',
            'password' =>'required|string|min:8|confirmed',
        ], [
            'name.required' =>'El campo nombre es obligatorio',
            'name.string' =>'El campo nombre debe ser texto',
            'name.max' =>'El campo nombre debe tener como maximo 255 caracteres',
            'email.required' =>'El campo email es obligatorio',
            'email.max' =>'El campo email debe tener como maximo 255 caracteres',
            'email.unique' =>'El campo email ya se encuentra en la base de datos',
            'email.email' =>'el campo email debe tener un formato valido',
            'password.required' =>'El campo contraseña es obligatorio',
            'password.string' =>'El campo contraseña debe ser texto',
            'password.min' =>'El campo contraseña debe tener como minimo 8 caracteres',
            'password.confirmed' =>'Las contraseñas deben coincidir',
        ]);

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        //$user->password = $data['password'];
        $user->save();

        return redirect('/cp/users')->with('success', 'El usuario se actualizo correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/cp/users')->with('success', 'El usuario se elimino con exito');
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $message = '';
        $user = User::find($id);
        if($user->active){
            $user->active = false;
            $message = 'desactivo';
        } else {
            $user->active =true;
            $message = 'activo';
        }
        $user->save();
        return redirect('/cp/users')->with('success', 'El usuario se '.$message.' con exito');
    }
}
