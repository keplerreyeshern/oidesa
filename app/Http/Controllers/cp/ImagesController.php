<?php

namespace App\Http\Controllers\cp;

use App\Http\Controllers\Controller;
use App\ImageGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //listar las imagenes
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('file');

        $path = "/galerias/" . $request->id;

        if( ! is_dir($path)){
            mkdir(public_path($path),  0777);
        }

        //$imageName = time().'.'. $image->extension();
        //$imageName =  str_slug($image->getClientOriginalName()) . '.' . $image->extension();

       $imageName =  basename($request->file('file')->getClientOriginalName()  , '.' . $image->extension());
       $imageName = str_replace(" ", "-", $imageName);

        $image->move(public_path($path), $imageName);

        //agregar a la base de datos



        return response()->json(['success'=>$imageName]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = ImageGallery::find($id);
        $image->delete();
        return $image;
    }
}
