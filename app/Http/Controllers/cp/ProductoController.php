<?php

namespace App\Http\Controllers\cp;

use App\Category;
use App\Gallery;
use App\ImageGallery;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['titulo'] = 'Productos';
        $data['modulo'] = 'producto';

        $products = Product::get();
        $categories = Category::where('type', 'products')->get();
        $galleries = Gallery::where('type', 'products')->get();
        $images = ImageGallery::get();
        $categoriesSelect = DB::table('category_product')->get();

        return view('cp.modulos.productos.index', compact(
            'data',
            'categories',
            'products',
            'galleries',
            'images',
            'categoriesSelect'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {
        $product = Product::find($request['id']);

        if ($request['new']){
            $product->new = false;
        } else {
            $product->new = true;
        }

        $product->save();

        return $product;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request)
    {
        $product = Product::find($request['id']);

        if ($request['active']){
            $product->active = false;
        } else {
            $product->active = true;
        }

        $product->save();

        return $product;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');
        $product = new Product();
        $product->name = $request['name'];
        $product->slug = Str::slug($request['name']);
        $product->key = $request['key'];
        $product->model = $request['model'];
//        $product->full_leather = $request['full_leather'];
//        $product->polyurethane = $request['polyurethane'];
        $product->description = $request['description'];
        $product->details = $request['details'];
        $product->measuresCms = $request['measuresCms'];
        $product->measuresInches = $request['measuresInches'];
        if ($request['full_leather'] == 'true'){
            $product->full_leather = 1;
        } else {
            $product->full_leather = 0;
        }
        if ($request['polyurethane'] == 'true'){
            $product->polyurethane = 1;
        } else {
            $product->polyurethane = 0;
        }
        if($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/products/' . $name,  \File::get($file));
            $product->photo = '/storage/images/products/'. $name;
        }
        $product->save();

        $categories = explode(",", $request['categories']);
        $product->categories()->sync($categories);

        $galleries = Gallery::whereNULL('reference')
            ->where('type', 'products')
            ->get();
        if(count($galleries) > 0){
            foreach ($galleries as $gallery){
                $gallery->reference = $product->id;
                $gallery->name = $product->name;
                $gallery->description = $product->description;
                $gallery->save();
            }
        }

        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['titulo'] = 'Productos / Editar';
        $data['modulo'] = 'producto';
        $data['accion'] = 'Editar';

        $data['action'] = 'productos/update';

        $producto = Producto::find($id);


        return view('cp.modulos.productos.form', compact('data', 'producto'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = $request->file('image');

        $product = Product::find($id);
        $product->name = $request['name'];
        $product->slug = Str::slug($request['name']);
        $product->key = $request['key'];
        $product->model = $request['model'];
        $product->description = $request['description'];
        $product->details = $request['details'];
        $product->measuresCms = $request['measuresCms'];
        $product->measuresInches = $request['measuresInches'];
        if ($request['full_leather'] == 'true'){
            $product->full_leather = 1;
        } else {
            $product->full_leather = 0;
        }
        if ($request['polyurethane'] == 'true'){
            $product->polyurethane = 1;
        } else {
            $product->polyurethane = 0;
        }
        if ($file) {
            $name = time() . "_" . $file->getClientOriginalName();
            Storage::disk('public')->put('/images/products/' . $name, \File::get($file));
            $product->photo = '/storage/images/products/'.$name;
        }
        $product->save();

        $categories = explode(",", $request['categories']);
        $product->categories()->sync($categories);

//        $result = [
//            'name' => $request['name'],
//            'key' => $request['key'],
//            'model' => $request['model'],
//            'description' => $request['description'],
//            'details' => $request['details'],
//            'measuresCms' => $request['measuresCms'],
//            'measuresInches' => $request['measuresInches'],
//            'photo' => $request['image'],
//            'categories' => $request['categories'],
//            'request' => $request,
//        ];

        return $categories;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return $product;
    }
}
