<?php

namespace App\Http\Controllers\cp;

use App\Gallery;
use App\ImageGallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GaleriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['titulo'] = 'Galerias';
        $data['modulo'] = 'galeria';

        $galleries = Gallery::get();
        $images = ImageGallery::get();

        return view('cp.modulos.galerias.index', compact(
            'data',
            'galleries',
            'images'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request)
    {
        $gallery = Gallery::find($request['id']);

        if ($request['active']){
            $gallery->active = false;
        } else {
            $gallery->active = true;
        }

        $gallery->save();

        return $gallery;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = $request['datetime'];
        $gallery = Gallery::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'datetime' => date("Y-m-d h:m:s", strtotime($date)),
        ]);

        $images = ImageGallery::whereNULL('gallery_id')
            ->where('user_id', auth()->user()->id)
            ->get();

        foreach ($images as $image){
            $image->gallery_id = $gallery->id;
            $image->save();
        }

        return $gallery;
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function images(Request $request)
    {
        $galleryId = null;
        if($request['type'] == 'products' || $request['type'] == 'releases'){
            if($request['gallery'] == '0'){
                $gallery = Gallery::create([
                    'name' => $request['name'],
                    'reference' => $request['reference'],
                    'description' => $request['description'],
                    'datetime' => date_create('now'),
                    'type' => $request['type'],
                ]);
                $galleryId = $gallery->id;
            } else {
                $galleryId = $request['gallery'];
            }
        }  else {
            $galleryId = $request['gallery'];
        }
        $name =  time()."_".$request->file->getClientOriginalName();
        Storage::disk('public')->put('/images/galleries/' . $name,  \File::get($request->file));

        ImageGallery::create([
            'name' => '/storage/images/galleries/'.$name,
            'gallery_id' => $galleryId,
            'user_id' => auth()->user()->id,
        ]);
        return response()->json(['success'=>'You have successfully upload file.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = Gallery::find($id);
        $date = $request['datetime'];
        $gallery->name = $request['name'];
        $gallery->description = $request['description'];
        $gallery->datetime = date("Y-m-d h:m:s", strtotime($date));
        $gallery->save();;

        $images = ImageGallery::whereNULL('gallery_id')
            ->where('user_id', auth()->user()->id)
            ->get();

        foreach ($images as $image){
            $image->gallery_id = $gallery->id;
            $image->save();
        }

        return $gallery;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::find($id);
        $gallery->delete();
        return $gallery;
    }
}
