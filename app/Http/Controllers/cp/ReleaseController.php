<?php

namespace App\Http\Controllers\cp;

use App\Category;
use App\Gallery;
use App\Http\Controllers\Controller;
use App\ImageGallery;
use App\Release;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class ReleaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['titulo'] = 'Comunicados';
        $data['modulo'] = 'Comunicados';

        //lista de todos los usuarios
        $releases = Release::get();

        return view('cp.modulos.releases.index', compact('data','releases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['titulo'] = 'Crear Comunicado';
        $data['modulo'] = 'Crear Comunicado';

        $categories = Category::where('type', 'releases')->get();
        $release = Release::get();
        $galleries = Gallery::where('type', 'releases')->get();
        $images = ImageGallery::get();

        return view('cp.modulos.releases.create', compact(
            'data',
            'categories',
            'release',
            'galleries',
            'images'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $photo_release = $request->file('photo_release');
        $photo_list = $request->file('photo_list');
        $file = $request->file('pdf');

        $release = new Release();
        $release->slug = Str::slug($request['title']);
        $release->title = $request['title'];
        $release->subtitle = $request['subtitle'];
        $release->intro = $request['intro'];
        $release->description = $request['description'];
        $release->date = $request['date'];
        $release->author = $request['author'];
        $release->category_id = $request['category_id'];

        if ($photo_list){
            $nameList =  time()."_".$photo_list->getClientOriginalName();
            Storage::disk('public')->put('/images/releases/' . $nameList,  \File::get($photo_list));
            $release->photo_list = '/storage/images/releases/'. $nameList;
        }
        if ($photo_release){
            $nameRelease =  time()."_".$photo_release->getClientOriginalName();
            Storage::disk('public')->put('/images/releases/' . $nameRelease,  \File::get($photo_release));
            $release->photo_release = '/storage/images/releases/'. $nameRelease;
        }
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/files/releases/' . $name,  \File::get($file));
            $release->pdf = '/storage/files/releasse/'. $name;
        }

        $release->save();

        return redirect('/cp/releases')->with('success', 'El comunicado se creo con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $release = Release::find($id);
        $data['titulo'] = 'Editar el Comunicado '.$release->title;
        $data['modulo'] = 'Editar el Comunicado '.$release->title;

        $categories = Category::where('type', 'releases')->get();
        $galleries = Gallery::where('type', 'releases')->get();
        $images = ImageGallery::get();

        return view('cp.modulos.releases.edit', compact(
            'data',
            'categories',
            'release',
            'galleries',
            'images'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $photo_release = $request->file('photo_release');
        $photo_list = $request->file('photo_list');
        $file = $request->file('pdf');

        $release = Release::find($id);
        $release->slug = $slug = Str::slug($request['title']);;
        $release->title = $request['title'];
        $release->subtitle = $request['subtitle'];
        $release->intro = $request['intro'];
        $release->description = $request['description'];
        $release->date = $request['date'];
        $release->author = $request['author'];
        $release->category_id = $request['category_id'];

        if ($photo_list){
            $nameList =  time()."_".$photo_list->getClientOriginalName();
            Storage::disk('public')->put('/images/releases/' . $nameList,  \File::get($photo_list));
            $release->photo_list = '/storage/images/releases/'. $nameList;
        }
        if ($photo_release){
            $nameRelease =  time()."_".$photo_release->getClientOriginalName();
            Storage::disk('public')->put('/images/releases/' . $nameRelease,  \File::get($photo_release));
            $release->photo_release = '/storage/images/releases/'. $nameRelease;
        }
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/files/releases/' . $name,  \File::get($file));
            $release->pdf = '/storage/files/releasse/'. $name;
        }

        $release->save();

        return redirect('/cp/releases')->with('success', 'El comunicado se actualizo con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $release = Release::find($id);
        $release->delete();

        return redirect('/cp/releases')->with('success', 'El comunicado se elimino con exito');
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $message = '';
        $user = User::find($id);
        if($user->active){
            $user->active = false;
            $message = 'desactivo';
        } else {
            $user->active =true;
            $message = 'activo';
        }
        $user->save();
        return redirect('/cp/releases')->with('success', 'El comunicado se '.$message.' con exito');
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function outstanding($id)
    {
        $message = '';
        $release = Release::find($id);
        if($release->outstanding){
            $release->outstanding = false;
            $message = 'quito destacado';
        } else {
            $release->outstanding = true;
            $message = 'hizo destacado';
        }
        $release->save();
        return redirect('/cp/releases')->with('success', 'El comunicado se '.$message.' con exito');
    }
}
