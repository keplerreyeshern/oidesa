<?php

namespace App\Http\Controllers\cp;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str as Str;

class ProductoCategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['titulo'] = 'Categorías';
        $data['modulo'] = 'Categorías';

        $categories = Category::where('type', 'products')->whereNull('parent_id')->get();

        return view('cp.modulos.productos.categorias.index', compact('data','categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     */
    public function subcategories($id)
    {
        $categories = Category::where('parent_id', $id)->get();
        return $categories;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::create([
            'name' => $request['name'],
            'slug' => Str::slug($request['name']),
            'parent_id' => $request['parent_id'],
            'type' => 'products',
        ]);

        return $category;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request['name'];
        $category->slug = Str::slug($request['name']);
        $category->parent_id = $request['parent_id'];
        $category->save();
        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return $category;
    }
}
