<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Gallery extends Model
{
    use SoftDeletes, HasTranslations;

    protected $table = "galleries";
    public $translatable = ['name', 'description'];

    protected $fillable = ['name', 'description', 'active', 'datetime', 'type', 'reference'];
}
