<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Release extends Model
{
    use SoftDeletes, HasTranslations;

    protected $table = "releases";


    public $translatable = [
        'title',
        'subtitle',
        'intro',
        'description',
    ];

    protected $fillable = [
        'slug',
        'title',
        'subtitle',
        'intro',
        'description',
        'author',
        'date',
        'active',
        'outstanding',
        'photo_release',
        'photo_list',
        'pdf',
        'category_id',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function categories(){
        return $this->belongsToMany('App\Category')->withTimestamps();
    }
}
