<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReleasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('releases', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->string('intro')->nullable();
            $table->string('description');
            $table->string('author')->nullable();
            $table->date('date')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('outstanding')->default(false);
            $table->string('photo_release')->nullable();
            $table->string('photo_list')->nullable();
            $table->string('pdf')->nullable();
            $table->foreignId('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('releases');
    }
}
