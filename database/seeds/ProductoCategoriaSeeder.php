<?php

use Illuminate\Database\Seeder;
use App\ProductoCategoria;

class ProductoCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        ProductoCategoria::create([
            'parent_id' => null,
            'name' => 'Espumados'
        ]);

        ProductoCategoria::create([
            'parent_id' => null,
            'name' => 'Sillas'
        ]);

        ProductoCategoria::create([
            'parent_id' => null,
            'name' => 'Sillones'
        ]);
    }
}
