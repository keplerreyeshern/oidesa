<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'Gerardo Trujillo',
            'email' => 'trujillo@excess.com.mx',
            'password' => Hash::make('trukas02'),
        ]);

        User::create([
            'name' => 'Kepler Reyes ',
            'email' => 'keplerreyeshern@gmail.com',
            'password' => Hash::make('Balam.0412'),
        ]);

    }
}
