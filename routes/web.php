<?php

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use \App\Mail\Contact;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('storage-link', function (){
    \Illuminate\Support\Facades\Artisan::call('storage:link');
});

Route::get('/', function () {

    return view('public.home');
})->name('index');

Route::get('/quienes-somos', function () {

    $data['titulo'] = 'Quienes Somos';

    return view('public.quienes-somos')->with('data', $data);
})->name('about');

Route::get('/garantia/piel-integral', function () {

    $data['titulo'] = 'Garantia Piel Integral';

    return view('public.garantia_piel')->with('data', $data);
})->name('full-leather');

Route::get('/garantia/poliuretano', function () {

    $data['titulo'] = 'Garantia Poliuretano';

    return view('public.garantia_poliuretano')->with('data', $data);
})->name('polyurethane');

Route::get('/politicas/privacidad', function () {

    $data['titulo'] = 'Politicas de Privacidad';

    return view('public.notice-of-privacy')->with('data', $data);
})->name('privacy');

Route::get('/contacto', function () {

    $data['titulo'] = 'Contacto';

    return view('public.contact')->with('data', $data);
})->name('contact');

Route::post('/contact', function (Request $request) {
    $data = $request->validate([
        'name' => 'required',
        'email' => 'required',
        'telephone' => 'required|string|min:10|max:10',
        'message' => 'required',
        'g-recaptcha-response' => 'required|captcha'
    ], [
        'name.required' => 'El campo nombre es obligatorio',
        'email.required' => 'El campo email es obligatorio',
        'phone.required' => 'El campo telefono es obligatorio',
        'phone.min' => 'El campo telefono debe tener 10 digitos',
        'phone.max' => 'El campo telefono debe tener 10 digitos',
        'message.required' => 'El campo mensaje es obligatorio',
        'g-recaptcha-response.required' => 'El captcha es obligatorio'
    ]);
    Mail::to(env('MAIL_TO_ADDRESS'))
        ->queue(new Contact($data));

    return redirect('contacto')->with('success', '¡El mensaje se envio con exito!');
})->name('send.contact');

Route::get('/productos/categoria/{category}', function ($category) {

    $category = \App\Category::where('slug', $category)->firstOrFail();
    $data['titulo'] = 'Productos de Categoria ' . $category->name;
    $products = \App\Category::findOrFail($category->id)->products()->get();

    foreach ($products as $product){
        $description = $product->description;
        $description = str::limit($description, 80);
        $index = 0;
        for($i=0;strlen($description)>$i;$i++){
            if($description[$i] == ' '){
                $index = $i;
            }
        }
        $description = substr($description, 0, $index);
        if (strlen($description) > 0){
            $description = $description . '...';
        }
        $product->description = $description;
    }

    return view('public.products_category', compact('data', 'category', 'products'));
})->name('products-category');

Route::get('/novedades', function () {

    $data['titulo'] = 'Novedades ';
    $products = \App\Product::where('active', true)->where('new', true)->get();

    return view('public.products_novelties', compact('data','products'));
})->name('novelties');

Route::get('/novedades/{product}', function ($slug)
{
    $product = \App\Product::where('slug', $slug)->firstOrFail();
    $data['titulo'] = $product->name;
    $gallery = \App\Gallery::where('reference', $product->id)->firstOrFail();
    if($gallery){
        $images = \App\ImageGallery::where('gallery_id', $gallery->id)->get();
    } else {
        $images = [];
    }

    return view('public.product_novelties', compact('data', 'product', 'images'));
})->name('product-novelties');

Route::get('/productos/{category}/{product}', function ($cate, $slug) {
    $category = \App\Category::where('slug', $cate)->firstOrFail();
    $product = \App\Product::where('active', true)->where('slug', $slug)->firstOrFail();
    $data['titulo'] = $product->name;
    $gallery = \App\Gallery::where('reference', $product->id)->first();
    if($gallery){
        $images = \App\ImageGallery::where('gallery_id', $gallery->id)->get();
    } else {
        $images = [];
    }

    return view('public.product', compact('data', 'product', 'images', 'category'));
})->name('product');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');




Route::middleware('auth')->group(function () {
    // Rutas con prefijo admin
    Route::group(['prefix'=>'cp', 'namespace' => 'cp', 'as'=>'cp.'], function(){

        Route::get('/', function () {
            $data['titulo'] = 'Bienvenida';

            return view('cp.cp', compact('data'));
        });


//        Route::post('/users/register', 'UsersController@store');
        Route::get('users/active/{id}', 'UsersController@active')->name('users.active');
        Route::get('users/destroy/{id}', 'UsersController@destroy')->name('users.destroy');

        Route::get('/users', 'UsersController@index')->name('users');
        Route::get('/users/create', 'UsersController@create')->name('users.create');
        Route::get('/users/{id}/edit', 'UsersController@edit')->name('users.edit');
        Route::post('/users', 'UsersController@store')->name('users.store');
        Route::put('/users/{id}', 'UsersController@update')->name('users.update');

        Route::resource('/producto-categorias', 'ProductoCategoriaController')->only([
            'index', 'store', 'update', 'destroy'
        ]);
        Route::get('/producto-categorias/{id}', 'ProductoCategoriaController@subcategories');


        Route::resource('/productos', 'ProductoController')->except([
            'show', 'create', 'edit'
        ]);

        Route::post('/productos/active', 'ProductoController@active');
        Route::post('/productos/new', 'ProductoController@new');


//        Route::resource('/releases', 'ReleaseController');
//        Route::get('/releases/destroy/{id}', 'ReleaseController@destroy')->name('releases.destroy');
//        Route::get('/releases/active/{id}', 'ReleaseController@active')->name('releases.active');
//        Route::get('/releases/outstanding/{id}', 'ReleaseController@outstanding')->name('releases.outstanding');


        Route::get('/register', function(){
            $data['titulo'] = 'Registrar Usuario';
            return view('auth.register', compact('data'));
        });

        //Productos controller


        //Images Controller
        Route::resource('/images', 'ImagesController')->only(
            'store', 'destroy'
        );

        //galerias controller
        Route::resource('galerias','GaleriasController')->only([
            'index', 'store', 'update', 'destroy'
        ]);
        Route::post('/gallery/store', 'GaleriasController@images');
        Route::post('/gallery/active', 'GaleriasController@active');



    }) ;
});
